-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.31-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for zigzag
CREATE DATABASE IF NOT EXISTS `zigzag` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `zigzag`;

-- Dumping structure for table zigzag.phone_numbers
CREATE TABLE IF NOT EXISTS `phone_numbers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_number` varchar(50) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `status` enum('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- Dumping data for table zigzag.phone_numbers: ~4 rows (approximately)
/*!40000 ALTER TABLE `phone_numbers` DISABLE KEYS */;
INSERT INTO `phone_numbers` (`id`, `phone_number`, `user_id`, `description`, `status`) VALUES
	(1, '09271285797', 5, 'unit test number', 'ACTIVE'),
	(2, '09271285791', 7, 'dsadasdad', 'ACTIVE');
/*!40000 ALTER TABLE `phone_numbers` ENABLE KEYS */;

-- Dumping structure for table zigzag.users_authentication
CREATE TABLE IF NOT EXISTS `users_authentication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `token` varchar(300) NOT NULL DEFAULT '0',
  `expired_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;

-- Dumping data for table zigzag.users_authentication: ~7 rows (approximately)
/*!40000 ALTER TABLE `users_authentication` DISABLE KEYS */;
INSERT INTO `users_authentication` (`id`, `user_id`, `token`, `expired_at`, `updated_at`) VALUES
	(1, 1, 'c2e96fb6fe3ae62ee14b9b474c70288389e8492d', '2019-10-26 06:55:11', '2018-10-26 02:55:11');
/*!40000 ALTER TABLE `users_authentication` ENABLE KEYS */;

-- Dumping structure for table zigzag.user_info
CREATE TABLE IF NOT EXISTS `user_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(300) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` set('ACTIVE','INACTIVE') NOT NULL DEFAULT 'ACTIVE',
  `name` varchar(300) NOT NULL,
  `type` varchar(45) NOT NULL DEFAULT '2',
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- Dumping data for table zigzag.user_info: ~4 rows (approximately)
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
INSERT INTO `user_info` (`id`, `username`, `password`, `status`, `name`, `type`, `last_login`) VALUES
	(1, 'admin', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'ACTIVE', 'Admin Zig Zag', '1', '2018-10-26 02:55:11'),
	(5, 'nicolasfloreszigzag', 'dc724af18fbdd4e59189f5fe768a5f8311527050', 'ACTIVE', 'zag zigsddsadada', '2', '2018-10-25 01:00:59');
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
