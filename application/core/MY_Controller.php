<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public $return_data = array('status' => 200, 'message' => '');

	public function __construct()
	{
		parent::__construct();
		$this->output->set_content_type('application/json');
	}

	protected function check_auth_client() {
		$client_service = $this->input->get_request_header('Client-Service', TRUE);
		$auth_key = $this->input->get_request_header('Auth-Key', TRUE);

		if($client_service != CLIENT_SERVICE  || $auth_key != AUTH_KEY ) {
			json_output(401, array('status' => 401, 'message' => 'Unauthorized.'));
			return false;
		} else {
			return true;
		}
	}

	protected function check_request_method($_method = 'POST') {
		$method = $_SERVER["REQUEST_METHOD"];
		if($method != $_method) {
			json_output(400, array('status' => 400, 'message' => 'Bad request.'));
			return false;
		} else {
			return true;
		}

	}

	protected function check_request_method_and_auth_client(){
		if(!$this->check_auth_client()){
			return false;
		}

		if(!$this->check_request_method()) {
			return false;
		}

		return true;

	}

	protected function authenticate_token() {
		if(!$this->check_request_method_and_auth_client()) {
			return false;
		} 

		$this->load->model('users_model', '', TRUE);
		$user_id = $this->input->get_request_header('User-Id', TRUE);
		$token = $this->input->get_request_header('Token', TRUE);

		$result = $this->users_model->authenticate_token($user_id, $token);

		if(!$result["success"]) {
			json_output($result["status"], array('status' => $result["status"], 'message' => $result["message"] ));
			return false;
		} else {
			return true;
		}
	}

	protected function get_authorized_user_info() {
		
		$user_id = $this->input->get_request_header('User-Id', TRUE);
		$token = $this->input->get_request_header('Token', TRUE);

		$this->load->model('users_model', '', TRUE);

		$_userinfo = $this->users_model->get_user_info($user_id);

		if($_userinfo != false) 
		{
			return $_userinfo;
		} else{
			json_output(404, array('status' => 404, 'message' => 'User information not found.',  'success'=> false));
		}
	}

	protected function set_default_value($value, $default = '') {
		return $value !== NULL ? $value : $default;
	}

	protected function get_array_value($array, $key, $default = false) {
		return isset($array[$key]) ? $array[$key] : $default;
	}

}
