<?php
class Users extends MY_Controller {

	public function __construct() {
		parent::__construct();
		// $this->check_auth_client();
		// $this->check_request_method();
	}

	/**
	 * --Note--
	 * User types:
	 * admin = 1
	 * non admin = 2
	 */
	
	/**
	 * Api for getting all user information
	 * @param page; number, default 1
	 * 
	 */
    public function get_user_list() 
    {
		//checking if the client is authorized
		if(!$this->authenticate_token()) {
			return;
		} 

		$_userinfo = $this->get_authorized_user_info();

		if($_userinfo->type != 1){
			json_output(401, array('status' => 401, 'message' => 'Access is denied',  'success'=> false));
			return;
		}

		$page = $this->set_default_value($this->input->get_post('page'), 1);

		$user_list = $this->users_model->get_active_user_list($page);

		json_output(200, array('status' => 200, 'message' => '', 'success' => true, 'current_page' => (int) $page, 'next_page' => $page + 1,  'user_list' => $user_list  ));
		
		return;
	}
	
	/**
	 * Api for getting current user information
	 */
    public function get_user_info() 
    {
		//checking if the client is authorized
		if(!$this->authenticate_token()) {
			return;
		} 

		$userinfo = $this->get_authorized_user_info();
		
		unset($userinfo->password);

		json_output(200, array('status' => 200, 'message' => '', 'success' => true, 'userinfo' => $userinfo  ));
		return;
	}

	/**
	 * Api for updating user information
	 * @param user_id string
	 * @param name string
	 * @param status "ACTIVE" or "INACTIVE" this param is for deleting
	 */
	public function update_user_info() 
    {	
		//checking if the client is authorized
		if(!$this->authenticate_token()) {
			return;
		} 

		$_userinfo = $this->get_authorized_user_info();

		$params = $_REQUEST;
		$user_id =  $this->set_default_value($this->input->get_post('user_id'));
		$name =  $this->set_default_value($this->input->get_post('name'));

		if(!$user_id) 
		{
			json_output(400, array('status' => 400, 'message' => 'No user_id was specified.',  'success'=> false));
			return;
		} 

		if(!$name) 
		{
			json_output(400, array('status' => 400, 'message' => 'No name was specified.',  'success'=> false));
			return;
		} 

		//if user is not admin and tries to update others information
		if($_userinfo->type != 1 && $_userinfo->id != $user_id) {
			json_output(401, array('status' => 401, 'message' => 'Access is denied. User is unauthorized or not able to update others information',  'success'=> false));
			return;
		}

		$user_to_update = $this->users_model->get_user_info($user_id);
		$user_status = $this->set_default_value($this->input->get_post('user_status', $user_to_update->status));
		
		//validate if status has a proper value	
		if($user_status != 'ACTIVE' && $user_status != 'INACTIVE') {
			json_output(400, array('status' => 400, 'message' => 'Invalid user status.',  'success'=> false));
			return;
		}

		$data = array(
			"name" => $name,
			"status" => $user_status,
		);

		if($this->users_model->update_user_info($_userinfo->id, $data)) {
			json_output(200, array('status' => 200, 'message' => 'Updating is successful', 'success' => true ));
			return;
		} else {
			json_output(500, array('status' => 500, 'message' => 'Internal Server Error.', 'success' => false ));
			return;
		}
	}

	/**
	 * Api for creating user
	 * @param username string required
	 * @param password string required
	 * @param cpassword string required
	 * @param name string required
	 * @param type 1 or 2  only admin can create admin role
	 */
	public function create_user() 
    {
		//checking if the client is authorized
		if(!$this->authenticate_token()) {
			return;
		} 
		$this->save_user(true);
	}
	
	/**
	 * Api for user registration, this can access publicly
	 * @param username string required
	 * @param password string required
	 * @param cpassword string required
	 * @param name string required
	 */
	public function user_registration() 
    {
		if(!$this->check_request_method_and_auth_client()) {
			return;
		} 
		$this->save_user();
	}

	/* Non api functions */	
	protected function save_user($check_access = false) 
    {
		$this->load->model('users_model', '', TRUE);
		$is_admin = false;
		if($check_access) {
			$_userinfo = $this->get_authorized_user_info();
			if($_userinfo->type == 1) {
				$is_admin = true;
			}
		}

		$status = 200;
		$message = '';
		$is_valid = true;
		$success = true;

		$username =  $this->set_default_value($this->input->get_post('username'));
		$password =  $this->set_default_value($this->input->get_post('password'));
		$cpassword =  $this->set_default_value($this->input->get_post('cpassword'));
		$name =  $this->set_default_value($this->input->get_post('name'));
		$type =  $this->set_default_value($this->input->get_post('type', 2));

		if(!$username) 
		{
			$is_valid = false;
			$status = 400;
			$message = 'No username was specified.';
		} 

		if($this->users_model->check_duplicate_username($username)) {
			$is_valid = false;
			$status = 400;
			$message = 'Duplicate username.';
		}

		if(!$name) 
		{
			$is_valid = false;
			$status = 400;
			$message = 'No name was specified.';
		} 

		if(!$password) 
		{
			$is_valid = false;
			$status = 400;
			$message = 'No password was specified.';
		} else {
			if($password != $cpassword) {
				$is_valid = false;
				$status = 400;
				$message = 'Passwords did not match';
			} 
		}

		if(!$is_admin && $type == 1) {
			$is_valid = false;
			$status = 401;
			$message = 'Access is denied. User is unauthorized or not able to create an admin user';
		} 

		if($is_valid) {

			$user_data = array( 'username' => $username, 'password' => sha1($password), 'name' => $name, 'type' => $type );
			
			if($this->users_model->create_user($user_data)) {
				$status  = 200;
				$message  = "Client Registration is successfull";
			}
		} 

		$success = $is_valid;

		json_output($status, array('status' => $status, 'message' => $message, 'success' =>  $success));
		return;
	}

}