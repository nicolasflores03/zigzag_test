<?php
class Phone_numbers extends MY_Controller {

	public function __construct() {
		parent::__construct();
		// $this->check_auth_client();
		// $this->check_request_method();
	}
	
	/**
	 * Api for getting all active phone numbers
	 * @param page; number default 1
	 * 
	 */
    public function get_phone_numbers_list() 
    {
		//checking if the client is authorized
		if(!$this->authenticate_token()){
			return;
		}

		$_userinfo = $this->get_authorized_user_info();

		$this->load->model('phone_numbers_model', '', TRUE);
		$user_id = false;
		if($_userinfo->type != 1){
			$user_id = $_userinfo->id;
		}

		$params = $_REQUEST;
		
		$page = isset($params['page']) ? $params['page'] : 1;

		$phone_numbers_list = $this->phone_numbers_model->get_active_phone_number_list($user_id, $page);

		json_output(200, array('status' => 200, 'message' => '', 'success' => true, 'current_page' => (int) $page, 'next_page' => $page + 1,  'phone_numbers_list' => $phone_numbers_list  ));

		return;
	}

	/**
	 * Api for creating phone number
	 * @param phone_number number required
	 * @param desctciption string optional
	 * @param user_id  default is own user id only admin can add others phone number
	 */
	public function create_phone_number() 
    {
		//checking if the client is authorized
		if(!$this->authenticate_token()){
			return;
		}

		$this->load->model('phone_numbers_model', '', TRUE);
		
		$_userinfo = $this->get_authorized_user_info();

		$status = 200;
		$message = '';
		$success = true;

		$params = $_REQUEST;
		
		$phone_number =  $this->set_default_value($this->input->get_post('phone_number'));
		$description =  $this->set_default_value($this->input->get_post('description', ''));
		$user_id =  $this->set_default_value($this->input->get_post('user_id', $_userinfo->id));

		$phone_number_data = array(
			"phone_number" => $phone_number,
			"description" => $description ,
			"user_id" => $user_id,
			"phone_status" => 'ACTIVE'
		);
		
		//if something is invalid it will automatically stop the process and return the error details
		if(!$this->validate_phone_number_info($phone_number_data)) {
			return;
		}

		$data = array( 'phone_number' => $phone_number, 'description' => $description, 'user_id' => $user_id);
		
		if($this->phone_numbers_model->create_phone_number($data)) {
			$status  = 200;
			$message  = "Adding is successfull";
			$success = true;
		}
		
		json_output($status, array('status' => $status, 'message' => $message, 'success' =>  $success));
		return ;
	}

	/**
	 * Api for updating and Deleting phone number
	 * @param phone_number number required
	 * @param desctciption string optional
	 * @param status "ACTIVE" or "INACTIVE" this param is for deleting
	 * @param user_id  default is own user id only admin can add others phone number
	 */
	public function update_phone_number() 
    {
		if(!$this->check_request_method_and_auth_client()) {
			return;
		} 

		//checking if the client is authorized
		if(!$this->authenticate_token()){
			return;
		}

		$this->load->model('phone_numbers_model', '', TRUE);
		
		$_userinfo = $this->get_authorized_user_info();

		$status = 200;
		$message = '';
		$is_valid = true;
		$success = true;

		$params = $_REQUEST;
		
		$phone_id =  $this->set_default_value($this->input->get_post('phone_id'));
		$phone_number =  $this->set_default_value($this->input->get_post('phone_number'));
		$description =  $this->set_default_value($this->input->get_post('description', ''));
		

		$phone_number_info = $this->phone_numbers_model->get_phone_number_info($phone_id);
	
		if(!$phone_number_info){
			$is_valid = false;
			$status  = 404;
			$message  = "No phone number information found.";
			$success = false;
		}



		$phone_status = $this->set_default_value($this->input->get_post('phone_status', $phone_number_info->status ));
		$user_id  = $phone_number_info->user_id;
		if($is_valid) {

			$phone_number_data = array(
				"phone_id" => $phone_id,
				"phone_number" => $phone_number,
				"description" => $description,
				"user_id" => $user_id,
				"phone_status" => $phone_status
			);
			
			//if something is invalid it will automatically stop the process and return the error details
			if(!$this->validate_phone_number_info($phone_number_data)) {
				return;
			}

			$data = array( 'phone_number' => $phone_number, 'description' => $description, 'user_id' => $user_id, "status" => $phone_status);
			
			if($this->phone_numbers_model->update_phone_number($phone_id, $data)) {
				$status  = 200;
				$message  = "Updating is successfull";
				$success = true;
			}
		} 

		json_output($status, array('status' => $status, 'message' => $message, 'success' =>  $success));
	}

	private function validate_phone_number_info($phone_number_data) {
		$_userinfo = $this->get_authorized_user_info();
		$is_admin = false;

		if($_userinfo->type == 1) {
			$is_admin = true;
		}

		$is_valid = true;
		$status = 200;
		$message = '';
		if(!$phone_number_data['phone_number']) 
		{
			$is_valid = false;
			$status = 400;
			$message = 'No phone number was specified.';
		}  else {
			$phone_id = $this->get_array_value($phone_number_data, 'phone_id', false); 
	
			if($this->phone_numbers_model->check_duplicate($phone_number_data['phone_number'], $phone_id)) {
				$is_valid = false;
				$status = 400;
				$message = 'Duplicate phone number.';
			}

			//checking if phone number is valid
			if( !preg_match('/^(?:09|\+?639)(?!.*-.*-)(?:\d(?:-)?){9}$/m', trim($phone_number_data['phone_number'])))   {
				$is_valid = false;
				$status = 400;
				$message = 'Invalid phone number.';
			}
		}

		if(!$phone_number_data['user_id']) 
		{
			$is_valid = false;
			$status = 400;
			$message = 'No user id was specified.';
		} 

		//validate if status has a proper value	
		if($phone_number_data['phone_status'] != false && $phone_number_data['phone_status'] != 'ACTIVE' && $phone_number_data['phone_status'] != 'INACTIVE') {
			$is_valid = false;
			$status = 400;
			$message = 'Invalid phone status.';
		}

		//validate if user has access to this phone details		
		if(!$is_admin && $phone_number_data['user_id'] != $_userinfo->id) {
			$is_valid = false;
			$status = 401;
			$message = 'Access is denied. User is unauthorized or not able to create/update others phone number';
		} 
		
		if(!$is_valid) {
			json_output($status, array('status' => $status, 'message' => $message, 'success' =>  $is_valid));
		} 

		return $is_valid ;
		
	}

}