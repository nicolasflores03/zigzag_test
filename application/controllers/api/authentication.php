<?php


class Authentication extends MY_Controller {

	public function __construct() {
		parent::__construct();
    }
    
    public function login() 
    {

		if(!$this->check_request_method_and_auth_client()) {
			return;
		} 

		$this->load->model('users_model', '', TRUE);


		$username = $this->input->get_post('username');
		$password = $this->input->get_post('password');
		$_userinfo = $this->users_model->user_details($username, $password);

		if($_userinfo != false) 
		{
			$tokeninfo = $this->users_model->generate_token($_userinfo);

			json_output(200, array('status' => 200, 'message' => 'Successfully login.', 'success' =>  TRUE,  'userinfo' => $tokeninfo));
			
		} else{

			json_output(403, array('status' => 403, 'message' => 'Invalid Username or password.', 'success' =>  FALSE));

		}
		
    }
}