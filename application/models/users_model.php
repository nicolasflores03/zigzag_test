<?php
	class Users_model extends CI_Model
	{
		public function __construct()
		{
			parent::__construct();
		}

		public function user_details($username, $password){
			$this->db->select('*');
			$this->db->from('user_info');
			$this->db->where('username', $username);
			$this->db->where('password', sha1($password));
			$this->db->where('status', 'ACTIVE');
			$this->db->limit(1);

			$query = $this->db->get();
			
			if($query->num_rows() > 0)
			{
				$result = $query->result();
				return $result[0];
			}
			else
			{
				return false;
			}
		}

		//authenticate token and update login details
		public function authenticate_token($user_id, $token)
    	{
			$this->db->select('expired_at');
			$this->db->from('users_authentication');
			$this->db->where('user_id', $user_id);
			$this->db->where('token', $token);
			
			$q = $this->db->get()->row();
			if($q == ""){
				return array('success' => false, 'status' => 401,'message' => 'Unauthorized.');
			} else {
				if($q->expired_at < date('Y-m-d H:i:s')){
					return array('status' => 401,'message' => 'Your session has been expired.');
				} else {
					$updated_at = date('Y-m-d H:i:s');
					$expired_at = date("Y-m-d H:i:s", strtotime('+4 hours'));
					$this->db->where('user_id', $user_id)->where('token',$token)->update('users_authentication', array('expired_at' => $expired_at,'updated_at' => $updated_at));
					return array('success' => true, 'status' => 200,'message' => 'Authorized.');
				}
			}
    	}

		public function generate_token($userinfo){
			$last_login = date('Y-m-d H:i:s');
			$token = sha1( crypt($userinfo->username . ' - ' . $userinfo->password, substr( md5(rand()), 0, 7) ) . $last_login );
			// $token = crypt( substr( md5(rand()), 0, 7) , $last_login);
			$expired_at = date("Y-m-d H:i:s", strtotime('+4 hours')); //adding expiration hours should be on constant/config
			$users_authentication = array('user_id' => $userinfo->id,'token' => $token,'expired_at' => $expired_at);

			$this->db->trans_start();
			$this->db->where('id',  $userinfo->id);
			$this->db->update('user_info', array('last_login' => $last_login)); 

			$this->db->insert('users_authentication', $users_authentication );
			if ($this->db->trans_status() === FALSE){
			   $this->db->trans_rollback();
			   return false;
			} else {
			   $this->db->trans_commit();
			   return $users_authentication ;
			}
		}

		public function create_user( $data){

			$this->db->insert('user_info', $data);

			return $this->db->affected_rows() > 0;
		}

		public function update_user_info($user_id, $data){

			$this->db->trans_start();
			$this->db->where('id',  $user_id);
			$this->db->update('user_info',$data);
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return false;
			 } else {
				$this->db->trans_commit();
				return true ;
			 }

			return $this->db->affected_rows() > 0;
		}
		
		public function get_user_info($user_id){

			$this->db->select('*');
			$this->db->from('user_info');
			$this->db->where('id',  $user_id);
			$query = $this->db->get();

			if($query->num_rows() > 0)
			{
				$result = $query->result();
				return $result[0];
			}
			else
			{
				return false;
			}

		}

		public function get_active_user_list($page, $num_per_page = 10){
			$offset = ($page * $num_per_page) ;
			$start = ($page * $num_per_page) - $num_per_page ;

			$this->db->select('*');
			$this->db->from('user_info');
			$this->db->where('status', 'ACTIVE');
			$this->db->order_by("name", "asc");
			$this->db->limit($offset, $start);

			$query = $this->db->get();

			return $query->result();

		}

		public function check_duplicate_username($username){
			$this->db->select('*');
			$this->db->from('user_info');
			$this->db->where('username', $username);
			$this->db->where('status', 'ACTIVE');
			$this->db->limit(1);

			$query = $this->db->get();
			
			if($query->num_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
	}
?>

