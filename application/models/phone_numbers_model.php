<?php
	class Phone_numbers_model extends CI_Model
	{
		public function __construct()
		{
			parent::__construct();
		}

		public function get_active_phone_number_list($user_id = false,$page, $num_per_page = 10){
			$offset = ($page * $num_per_page) ;
			$start = ($page * $num_per_page) - $num_per_page ;

			$this->db->select('phone_numbers.id, phone_numbers.phone_number, phone_numbers.user_id, phone_numbers.description, phone_numbers.status, user_info.username, user_info.name');
			$this->db->from('phone_numbers');
			$this->db->join('user_info', 'user_info.id = phone_numbers.user_id');
			$this->db->where('phone_numbers.status', 'ACTIVE');
			$this->db->where('user_info.status', 'ACTIVE');
			
			if($user_id) {
				$this->db->where('phone_numbers.user_id', $user_id);
			}

			$this->db->order_by("name", "asc");

			$this->db->limit($offset, $start);

			$query = $this->db->get();

			return $query->result();

		}

		public function create_phone_number($data){

			$this->db->insert('phone_numbers', $data);

			return $this->db->affected_rows() > 0;
		}

		public function update_phone_number($phone_id, $data){

			$this->db->trans_start();
			$this->db->where('id',  $phone_id);
			$this->db->update('phone_numbers',$data);
			
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return false;
			 } else {
				$this->db->trans_commit();
				return true ;
			 }

			return $this->db->affected_rows() > 0;
		}

		public function check_duplicate($phone_number, $phone_id = false){
			$this->db->select('*');
			$this->db->from('phone_numbers');
			$this->db->where('phone_number', $phone_number);
			$this->db->where('status', 'ACTIVE');
			
			if($phone_id != false) {
				$this->db->where('id !=', $phone_id);
			}

			$this->db->limit(1);

			$query = $this->db->get();
			
			if($query->num_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function get_phone_number_info($phone_id){
			
			$this->db->select('*');
			$this->db->from('phone_numbers');
			
			if($phone_id != false) {
				$this->db->where('id', $phone_id);
			}

			$this->db->limit(1);

			$query = $this->db->get();
			
			if($query->num_rows() > 0)
			{
				$result = $query->result();
				return $result[0];
			}
			else
			{
				return false;
			}
		}
		
	}
?>

