<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	function json_output($statusHeader,$response)
	{
		// header('Content-Type: application/json');
		// $this->output->set_content_type('application/json');

		// set_status_header($statusHeader);
		// echo json_encode($response);
		// exit();
		$ci =& get_instance();
		$ci->output->set_content_type('application/json');
		$ci->output->set_status_header($statusHeader);
		$ci->output->set_output(json_encode($response));
		// echo json_encode($response);
	}
	
?>