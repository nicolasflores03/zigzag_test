<?php


class Api_test extends TestCase {

	public function __construct() {
		parent::__construct();

		$this->resetInstance();
		
		//setting default auth-key and client-service
		$this->request->setHeader('Auth-Key', AUTH_KEY);
		$this->request->setHeader('Client-Service', CLIENT_SERVICE);
	}
	
	//test api authorization
	public function test_unauthorized_access_to_any_API() 
    {
		$this->resetInstance();
		$this->request->setHeader('Auth-Key', "randomstring");
		$this->request->setHeader('Client-Service', "randomstringagain");

		$output = $this->request(
			'POST',
			'api/authentication/login',
			[
				'username' => 'admin', 
				'password' => 'test'			
			]
		);

		$this->assertContains('{"status":401,"message":"Unauthorized."}', $output);
	}
	
	//login test
    public function test_login_200() 
    {
		$this->request(
			'POST',
			'api/authentication/login',
			[
				'username' => 'admin', 
				'password' => 'test'			
			]
		);

		$this->assertResponseCode(200);	
	}
	
	public function test_login_success() 
    {
		$output = $this->request(
			'POST',
			'api/authentication/login',
			[
				'username' => 'admin', 
				'password' => 'test'			
			]
		);

		$actual = json_decode($output);

		$expected = true;

		$this->assertEquals(
			$expected,
			$actual->success,
			'Your login was unsuccessful. Check your username or password'
		);
	}
	//end of login test

	//user api tests

	//get user list only admin can have access on this
	public function test_get_user_list() 
    {
		//need to change incase token is expired
		$this->request->setHeader('User-Id', 1); //userid of admin
		$this->request->setHeader('token', "c2e96fb6fe3ae62ee14b9b474c70288389e8492d");

		$output = $this->request(
			'POST',
			'api/users/get_user_list'
		);

		$actual = json_decode($output);

		$expected = true;

		$this->assertEquals(
			$expected,
			$actual->success,
			'Getting user list was unsuccessful'
		);
	}

	//get current user info
	public function test_get_user_info() 
    {
		//need to change incase token is expired
		$this->request->setHeader('User-Id', 1); //userid of admin
		$this->request->setHeader('token', "c2e96fb6fe3ae62ee14b9b474c70288389e8492d");

		$output = $this->request(
			'POST',
			'api/users/get_user_info'
		);

		$actual = json_decode($output);

		$expected = true;

		$this->assertEquals(
			$expected,
			$actual->success,
			'Getting user info was unsuccessful'
		);
	}

	//user registration
	public function test_user_registration() 
    {
		//random name and password
		$name = crypt( substr( md5(rand()), 0, 100) , date('Y-m-d H:s:'));
		$password = crypt( substr( md5(rand()), 0, 100) , date('Y-m-d H:s:'));
		
		$output = $this->request(
			'POST',
			'api/users/user_registration',
			[
				'username' => $name, 
				'name' => 'unit test', 
				'password' => $password,
				'cpassword' => $password
			]
		);

		$actual = json_decode($output);

		$expected = true;

		$this->assertEquals(
			$expected,
			$actual->success,
			'Your registration was unsuccessful'
		);
	}

	//user registration duplicate test
	public function test_user_registration_duplicate() 
    {
		$name = "admin";
		$password = "password";
		
		$output = $this->request(
			'POST',
			'api/users/user_registration',
			[
				'username' => $name, 
				'name' => 'unit test', 
				'password' => $password,
				'cpassword' => $password
			]
		);

		$this->assertContains('{"status":400,"message":"Duplicate username.","success":false}', $output);
	}

	//user info update
	public function test_user_update() 
    {
		//need to change incase token is expired
		$this->request->setHeader('User-Id', 1); //userid of admin
		$this->request->setHeader('token', "c2e96fb6fe3ae62ee14b9b474c70288389e8492d");

		$name = crypt( substr( md5(rand()), 0, 100) , date('Y-m-d H:s:'));
		$password = crypt( substr( md5(rand()), 0, 100) , date('Y-m-d H:s:'));
		
		$output = $this->request(
			'POST',
			'api/users/update_user_info',
			[
				'user_id' => 1, 
				'name' => 'Admin Zig Zag', 
				'user_status' => 'ACTIVE'
			]
		);

		$this->assertContains('{"status":200,"message":"Updating is successful","success":true}', $output);
	}
	//end of user api test


	//phone number api test
	//get phone numbner
	public function test_get_phone_number_list() 
    {
		//need to change incase token is expired
		$this->request->setHeader('User-Id', 1); //userid of admin
		$this->request->setHeader('token', "c2e96fb6fe3ae62ee14b9b474c70288389e8492d");

		$output = $this->request(
			'POST',
			'api/phone_numbers/get_phone_numbers_list',
			['page' => 1]
		);

		$actual = json_decode($output);

		$expected = true;

		$this->assertEquals(
			$expected,
			$actual->success,
			'Getting user list was unsuccessful'
		);
	}

	//test create phone number with duplicate 
	public function test_create_phone_number_duplicate() 
    {
		//need to change incase token is expired
		$this->request->setHeader('User-Id', 1); //userid of admin
		$this->request->setHeader('token', "c2e96fb6fe3ae62ee14b9b474c70288389e8492d");
		
		$output = $this->request(
			'POST',
			'api/phone_numbers/create_phone_number',
			[
				'phone_number' => '09271285797', 
				'description' => 'unit test number', 
				'user_id' => 5 
			]
		);
		
		$this->assertContains('{"status":400,"message":"Duplicate phone number.","success":false}', $output);
	}


	//test update phone number with duplicate 
	public function test_update_phone_number() 
    {
		//need to change incase token is expired
		$this->request->setHeader('User-Id', 1); //userid of admin
		$this->request->setHeader('token', "c2e96fb6fe3ae62ee14b9b474c70288389e8492d");
		
		$output = $this->request(
			'POST',
			'api/phone_numbers/update_phone_number',
			[
				'phone_id' => 1,
				'phone_number' => '09271285797', 
				'description' => 'unit test number', 
				'phone_status' => 1
			]
		);
		
		$this->assertContains('{"status":200,"message":"Updating is successfull","success":true}', $output);
	}
	//end of phone number api test

	
	
}