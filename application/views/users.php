<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Users</h1>
</div>
<div class="col-md-12">
    <div class="table-responsive">        
        <table class="table table-bordered obligation-data-table" >
            <thead>
                <tr>
                    <th style="width:150px;">Username</th>
                    <th style="width:80px;">Name</th>
                    <th style="width:150px;">Type</th>
                    <th style="width: 100px;">Status</th>
                    <th style="width:100px; text-align: center;">Action</th>
                </tr>
            </thead>
            
        </table>
    </div>
</div>

<script type="text/javascript">

$(document).ready(function() {
    
    var data_columns = new Array();
        data_columns.push({data: 'username'});
        data_columns.push({data: 'name'});
        data_columns.push({data: 'type'});
        data_columns.push({data: 'status'});
        data_columns.push({data: 'actions'});

    $('.obligation-data-table').DataTable( {
        'paging'      : true,
        'lengthChange': false,
        'ordering'    : false,
        'info'        : true,
        'autoWidth'   : false,
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: "<?=base_url();?>obligations/get_dt_obligation_list?fund_id=<?=$fund_id?>&year=<?=$year?>&legal_basis_id=<?=$legal_basis_id?>",
            type: 'POST'
        },
        'iDisplayLength': 10,
        "columns": data_columns,
       
    } );
</script>