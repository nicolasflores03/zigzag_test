<!DOCTYPE html>
<html>
<style>
.login-logo, .register-logo {
    font-size: 35px;
    text-align: center;
    margin-bottom: 25px;
    font-weight: 300;
}
.login-box, .register-box {
    width: 360px;
    margin: 7% auto;
}
body {font-family: Arial, Helvetica, sans-serif;}
form {border: 3px solid #f1f1f1;     width: 500px;}

input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

button:hover {
    opacity: 0.8;
}

.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
}


.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

</style>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="login-box-body">
  <h2>Login Form</h2>

     <form action="/action_page.php">
        
        <div class="container" >
          <label for="uname"><b>Username</b></label>
          <input type="text" placeholder="Enter Username" id="username" name="username" required>

          <label for="psw"><b>Password</b></label>
          <input type="password" placeholder="Enter Password" id="password" name="password" required>
              
          <button type="button" id="btnlogin">Login</button>
        </div>

       
      </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

  <!-- jQuery 3 -->
  <script src="<?=base_url();?>assets/js/jquery.min.js"></script>
  <script type="text/javascript">
    var baseurl = "<?php echo base_url();?>";	
      $(document).ready(function(){
        $('#btnlogin').on('click', function(){
          $(this).addClass("disabled");	
          login();		
        });

        $("#password").keypress(function(e) {
            if(e.which == 13) {
              $("#btnlogin").addClass("disabled");	
              login();	
            }
        });
      });	

      function login()
      {
        var username = $('#username').val();
        var password = $('#password').val();				
        $.ajax({
          type: "POST",
          url: baseurl + "authentication/login",
          data: {
            username: username,
            password: password
          },
          success: function(e){				
            $('#btnlogin').removeClass("disabled");
            if(e != "Completed"){	
              alert(e);
            } else{
              window.location = baseurl ;
            }
          }				
        });
      }
  </script>
</body>
</html>
